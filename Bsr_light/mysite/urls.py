from django.contrib import admin
from django.urls import include, path

from django.contrib import admin
import polls.views
from polls.models import Wiec

from django.shortcuts import redirect

def Auth_check(f):
	def Auth_check2(request, *args, **kwargs):
	    if request.user.is_authenticated:
	        return f(request, *args, **kwargs)
	    else:
	        return redirect("polls:login")
	return Auth_check2

def Active():
	def Active2(request, *args, **kwargs):
	    if len(Wiec.objects.filter(Active=True)) == 0:
	        return polls.views.Base.Home(request, *args, **kwargs)
	    else:
	    	return redirect("polls:queue")
	return Active2

urlpatterns = [
    path('', Auth_check(Active()), name='home'),
    path('polls/', include('polls.urls'),),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
]
