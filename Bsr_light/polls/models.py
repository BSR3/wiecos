import datetime
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

class Wiec(models.Model):
    Title = models.CharField(max_length=200)
    StartDate = models.DateTimeField()
    Active = models.BooleanField(default=False)
    Queue_active = models.BooleanField(default=False)
    Finnished = models.BooleanField(default=False)
    Summary = models.BooleanField(default=False)
    
    def __str__(self):
        return self.Title

class Topic(models.Model):
    topic_text = models.CharField(max_length=200)
    verified = models.BooleanField(default=False)
    user =  models.ForeignKey(User, on_delete=models.CASCADE)
    # description = models.CharField(null=True, max_length=500)
    # contribution = models.CharField(null=True, max_length=500)
    # help = models.CharField(null=True, max_length=50)
    # introduction = models.BooleanField(default=False)
    # money = models.BooleanField(default=False)

    wiec = models.ForeignKey(Wiec, null=True, on_delete=models.CASCADE)
    def __str__(self):
        return self.topic_text

class Announcement(models.Model):
    announcement_text = models.CharField(max_length=200)
    verified = models.BooleanField(default=False)
    user =  models.ForeignKey(User, on_delete=models.CASCADE)
    # description = models.CharField(null=True, max_length=500)

    wiec = models.ForeignKey(Wiec, null=True, on_delete=models.CASCADE)
    def __str__(self):
        return self.announcement_text

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    multiple_votes = models.BooleanField(default=False)
    ranked_system = models.BooleanField(default=False)
    active = models.BooleanField(default=False)

    wiec = models.ForeignKey(Wiec, on_delete=models.CASCADE)

    class Meta:
        get_latest_by='pub_date'
            
    def __str__(self):
        return self.question_text
    
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'
    
    
class Choice(models.Model):
    id = models.AutoField(primary_key=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    
    votes = models.IntegerField(default=0)
    owner = models.ManyToManyField(User, through='Vote')

    def __str__(self):
        return self.choice_text

class Vote(models.Model):
    #id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    answer = models.ForeignKey(Choice, on_delete=models.CASCADE)
    points = models.IntegerField(default=0)

    def __str__(self):
        return  'user id: {} and answer id: {}'.format(self.user.id, self.answer.id)

    class Meta:
        unique_together = [['user', 'answer']]
    

class Queue(models.Model):
    number = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    now = models.IntegerField(default=None, blank=True, null=True)
    
    def __str__(self):
        return self.user.username




