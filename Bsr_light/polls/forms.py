from django.forms import ModelForm
from django import forms
from .models import Question, Choice, Topic, Wiec, Announcement

class UserForm(forms.Form):
	username = forms.CharField(label="login", max_length=100, required=True)
	password = forms.CharField(label="password", max_length=100, required=True)

class GhostUserForm(forms.Form):
	username = forms.CharField(label="login", max_length=20, required=True)

class QuestionForm(ModelForm):

    class Meta:
        model = Question
        fields = ['question_text', 'ranked_system']

class ChoiceForm(ModelForm):

	class Meta(object):
		model = Choice
		fields = ['choice_text']

class TopicForm(ModelForm):

	class Meta(object):
		model = Topic
		fields = ['topic_text']

class AnnouncementForm(ModelForm):

	class Meta(object):
		model = Announcement
		fields = ['announcement_text']

class WiecForm(ModelForm):

	class Meta(object):
		model = Wiec
		fields = ['Title', 'StartDate']
