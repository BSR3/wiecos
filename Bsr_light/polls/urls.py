from django.urls import path
from django.http import HttpResponseRedirect, HttpResponseForbidden
from . import views
from django.shortcuts import redirect
from .models import Question, Vote
from django.urls import reverse
from django.contrib.auth.models import User
from .models import Wiec

def Active_wiec(f):
	def Active2(request, *args, **kwargs):
		if Wiec.objects.filter(Active=True).exists():
			return f(request, *args, **kwargs)
		else:
			return redirect("home")
	return Active2

def not_Finnished_wiec(f):
	def Active2(request, *args, **kwargs):
		if Wiec.objects.filter(Finnished=False).exists():
			return redirect("polls:activeWiec")
		else:
			return f(request, *args, **kwargs)

	return Active2

def active_voting(f):
	def active_voting2(request, *args, **kwargs):
		active_question = Question.objects.filter(active=True).exists()
		user = User.objects.get(username = request.user)
		Prezydium_bool = user.groups.filter(name="Prezydium").exists()
		if active_question == True and Prezydium_bool == False:
			q = Question.objects.filter(active=True)
			for question in q:
				for answer in question.choice_set.filter():
					if Vote.objects.filter(user=user, answer = answer).exists():
						return f(request, *args, **kwargs)
			return HttpResponseRedirect(reverse("polls:detail", args=(question.id,)))
		else:
			return f(request, *args, **kwargs)
	return active_voting2


def prezydium_auth(f):
	def prezydium_auth2(request, *args, **kwargs):
		Prezydium_bool = User.objects.get(username = request.user).groups.filter(name="Prezydium").exists()
		if Prezydium_bool:
				return f(request, *args, **kwargs)
		else:
			return views.ErrorView.Forbidden(request)
	return prezydium_auth2

def organizator_auth(f):
	def organizator_auth2(request, *args, **kwargs):
		organizator_bool = User.objects.get(username = request.user).groups.filter(name="Organizator").exists()
		if organizator_bool:
				return f(request, *args, **kwargs)
		else:
			return HttpResponseForbidden()
	return organizator_auth2

def Auth_check(f):
	def Auth_check2(request, *args, **kwargs):
		if request.user.is_authenticated:
			return f(request, *args, **kwargs)
		else:
			return redirect("polls:login")
	return Auth_check2

app_name = 'polls'
urlpatterns = [

	path('', Auth_check(views.Base.Home), name='Home'),
	path('data', Auth_check(views.Base.json), name='BaseData'),
	path('noWiec', Auth_check(views.Base.noWiec), name='noWiec'),
	path('activeWiec', Auth_check(views.Base.activeWiec), name='activeWiec'),

	path("login", views.Authorization.login_view, name="login"),
	path('logout', views.Authorization.logout_view, name='logout'),

	path('wiec_generate', Auth_check(not_Finnished_wiec(organizator_auth(views.WiecArchive.create))), name='wiec_generate'),

	path('generate', Auth_check(Active_wiec(prezydium_auth(views.GeneratePolls.view))), name='generate'),
	path('generate_firstVote', Auth_check(Active_wiec(prezydium_auth(views.GeneratePolls.firstVote))), name='generate_firstVote'),


	path('queue',  Auth_check(Active_wiec(active_voting(views.QueueView.view))), name='queue'),

	path('queue/open', Auth_check(prezydium_auth(views.QueueView.Open)), name='StartQueue'),
	path('queue/close', Auth_check(prezydium_auth(views.QueueView.Close)), name='StopQueue'),


	path('queue/data', Auth_check(views.QueueView.Json), name='Qdata'),
	path('queue/add', Auth_check(views.QueueView.add), name='add'),
	path('queue/delete', Auth_check(views.QueueView.delete), name='delete'),
	path('queue/delete_first', Auth_check(views.QueueView.delete_first), name='delete_first'),

	path('wiecJson/', Auth_check(views.WiecArchive.json), name='wiecJson'),

	path('wiec/<int:wiec_id>/', Auth_check(views.WiecArchive.index), name='wiecIndex'),
	path('StartWiec/', Auth_check(views.WiecArchive.StartWiec), name='StartWiec'),
	path('StopWiec/', Auth_check(views.WiecArchive.StopWiec), name='StopWiec'),
	path('wiec_summary/<int:wiec_id>/', Auth_check(views.WiecArchive.summary), name='wiecSummary'),

	path('Verify/verify_index/<int:wiec_id>/', Auth_check(views.Verify.manager_index), name='verifyManager_index'),
	path('Verify/index/<int:wiec_id>/', Auth_check(views.Verify.index), name='verifyIndex'),
	path('Verify/manager/<int:id>/<int:to_verify>/', Auth_check(views.Verify.manager), name='verifyManager'),

	path('<int:question_id>/', Auth_check(views.Polls.detail), name='detail'),

	path('<int:pk>/results/', Auth_check(active_voting(views.Polls.results)), name='results'),
	path('voted/', Auth_check(views.Polls.voted), name='voted'),
	
	path('<int:pk>/again/', Auth_check(views.Polls.again), name='again'),
	path('<int:pk>/again/data/', Auth_check(views.Polls.Json), name='data'),

	path('<int:question_id>/vote/', Auth_check(views.Polls.vote), name='vote'),
	path('close/', Auth_check(views.Polls.Close), name='StopVote'),

	path('<int:question_id>/results_gen/', Auth_check(views.GeneratePolls.results), name='results_gen'),

]
