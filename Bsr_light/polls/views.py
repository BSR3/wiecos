from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.http import HttpResponseRedirect

from .models import Question, Choice, Vote, Queue, Wiec, Announcement, Topic
from django.utils import timezone
from datetime import datetime
from django.http import JsonResponse
from time import time
from django.contrib.auth import logout 
import babel.dates

from django.contrib.auth import login as LOGIN
from django.contrib.auth.backends import BaseBackend
import requests
from .forms import QuestionForm, TopicForm, WiecForm, AnnouncementForm, UserForm, GhostUserForm
from django.contrib.auth.models import User
from django.forms import inlineformset_factory
import random
import string
from django.contrib.auth.models import Group
from django.contrib.auth import authenticate as Django_Authenticate
from django.contrib.auth.hashers import check_password

#class associated with basic pages and base.html
class Base:
	#FirstPage
	def Home(request):
		if Wiec.objects.filter(Active=True).exists():
			return redirect("polls:queue")
		else:
			#newest wiec
			wiec = Wiec.objects.order_by('-id').first()

			if wiec != None and wiec.Finnished == False:
				user = User.objects.get(username=request.user)

				announcement = Announcement(wiec=wiec, user=user)
				topic = Topic(wiec=wiec, user=user)

				if wiec.StartDate.minute < 10:
					m = '0' + str(wiec.StartDate.minute)
				else:
					m = wiec.StartDate.minute

				if request.method == "POST":
					Aform = AnnouncementForm(request.POST, instance=announcement)
					Tform = TopicForm(request.POST, instance=topic)

					topic_saved = False
					announcement_saved = False

					if Aform.is_valid():
						Aform.save()
						announcement_saved = True

					if Tform.is_valid():
						Tform.save()
						topic_saved = True

					return render(request, 'first.html', {"topic_saved" : topic_saved, "announcement_saved" : announcement_saved,"topic_saved" : topic_saved, "announcement_saved" : announcement_saved, "Title" : wiec.Title, "Date" : datetime.strftime(wiec.StartDate, "%d.%m.20%y"), "Hour" : wiec.StartDate.hour, "Minute" : m, "Active" : wiec.Active, "Planned" : True, "Aform" : Aform, "Tform" : Tform} )
				else:
					Aform = AnnouncementForm(instance=wiec)
					Tform = TopicForm(instance=wiec)

				return render(request, 'first.html', {"Title" : wiec.Title, "Date" : datetime.strftime(wiec.StartDate, "%d.%m.20%y"), "Hour" : wiec.StartDate.hour, "Minute" : m, "Active" : wiec.Active, "Planned" : True, "Aform" : Aform, "Tform" : Tform} )
			else:
				user = User.objects.get(username=request.user)
				announcement = Announcement(user=user)
				topic = Topic(user=user)
				
				if request.method == "POST":
					Aform = AnnouncementForm(request.POST, instance=announcement)
					Tform = TopicForm(request.POST, instance=topic)

					topic_saved = False
					announcement_saved = False

					if Aform.is_valid():
						Aform.save()
						announcement_saved = True

					if Tform.is_valid():
						Tform.save()
						topic_saved = True

					return render(request, 'first.html', {"topic_saved" : topic_saved, "announcement_saved" : announcement_saved, "Aform" : Aform, "Tform" : Tform} )
				else:
					Aform = AnnouncementForm()
					Tform = TopicForm()
				return render(request, 'first.html', {"Aform" : Aform, "Tform" : Tform} )
	
	#system of recognizing when a vote ended on base.html
	def json(request):
		if Question.objects.filter(active=True).exists():
			active_question = True
		else:
			active_question = False
		return JsonResponse({"active_question" : active_question})

	def noWiec(request):
		return render(request, "polls/noWiec.html")

	def activeWiec(request):
		return render(request, "polls/activeWiec.html")

#class with all things related to wiec object
class WiecArchive:
	def create(request):
		wiec = Wiec()
		if request.method == "POST":
			Wform = WiecForm(request.POST, instance=wiec)
			if Wform.is_valid():
				Wform.save()

				for topic in Topic.objects.filter(wiec=None):
					topic.wiec = wiec
					topic.save()

				for announcment in Announcement.objects.filter(wiec=None):
					announcment.wiec = wiec
					announcment.save()

			
			return  HttpResponseRedirect(reverse("polls:Home")) 
		else:
			Wform = WiecForm(instance=wiec)
		return render(request, 'polls/wiec_generate.html', {'Wform' : Wform})

	#index of a single wiec
	def index(request, wiec_id):
		wiec = Wiec.objects.filter(id = wiec_id).first()
		wiec_question_list = Question.objects.filter(wiec = wiec)
		
		for question in wiec_question_list:
			question.pub_date = babel.dates.format_datetime(question.pub_date, 'd MMMM yyyy HH:mm', locale='pl_PL')


		wiec = Wiec.objects.filter(id = wiec_id).first()
		wiec_topic_list = Topic.objects.filter(wiec = wiec)

		# order by voted order 
		verified = wiec_topic_list.filter(verified=True)
		print(wiec.Summary)
		return render(request, 'polls/wiec_index.html', {'Summary' : wiec.Summary, 'id' : wiec.id, 'wiec_question_list' : wiec_question_list, "Title" : wiec.Title, "Date" : datetime.strftime(wiec.StartDate, "%d.%m.20%y"), "verified" : verified} )

	#Json used at base.html
	def json(request):
		latest_wiec = None
		list_wiec = None
		if Wiec.objects.all().exists():
			wiec = Wiec.objects.order_by('-id').first()
			latest_wiec = {"id" : wiec.id, "Finnished" : wiec.Finnished, "Active" : wiec.Active}

			
			list_wiec = Wiec.objects.order_by('-id').values()
			return JsonResponse({"Wiece" : list(list_wiec), "Questions" : list(Question.objects.values()), "latest_wiec" : latest_wiec,})
		return JsonResponse({"Wiece" : None, "Questions" : None, "latest_wiec" : None,})

	def StartWiec(request):
		latest_wiec = Wiec.objects.order_by('-id').first()
		latest_wiec.Active = True
		latest_wiec.save()
		return redirect('polls:queue')
		#return redirect('polls:generate_firstVote')	KIEDYŚ DO TEGO WRÓCIMY

	def StopWiec(request):
		latest_wiec = Wiec.objects.order_by('-id').first()
		latest_wiec.Queue_active = False
		latest_wiec.save()

		for question in Question.objects.filter(active=True):
			question.active = False
			question.save()

		for queue in Queue.objects.all():
			queue.delete()
			
		latest_wiec.Active = False
		latest_wiec.Finnished = True
		latest_wiec.save()

		return redirect('polls:Home')

	def summary(request, wiec_id):
		return render(request, f'polls/WiecDescription/{wiec_id}_Description.html')

#class with all things related to queue object
class QueueView:
	#The most complicated page Award
	def view(request):
		user = User.objects.get(username=request.user)
		
		wiec = Wiec.objects.order_by('-id').first()
		topic = Topic(wiec=wiec, user=user)
		
		first = Queue.objects.all().order_by('id').first()
		if Queue.objects.all().exists():

			if first.user.groups.filter(name="Ghost").exists():
				first = {"username" : "Uczestnik wiecu", "id" : first.user.id}
			else:
				if first.user.first_name == "":
					first = {"username" : first.user.username, "id" : first.user.id}
				else:
					first = {"username" : f'{first.user.first_name} {first.user.last_name}',"id" : first.user.id}

		q = Queue.objects.filter(user = user).first()
				
		questions = Question.objects.filter(wiec=wiec).order_by("-id")

		active = {}

		if Question.objects.filter(active=True).exists():
			active['vote'] = True
		else:
			active['vote'] = False

		if Wiec.objects.filter(Queue_active=True).exists():
			active['queue'] = True
		else:
			active['queue'] = False

		if Wiec.objects.filter(Active=True).exists():
			active['wiec'] = True
		else:
			active['wiec'] = False

		if Announcement.objects.filter(user=user.id, verified=True, wiec=wiec).exists():
			personal_announcement = Announcement.objects.filter(user=user.id, verified=True, wiec=wiec)
		else:
			personal_announcement = None

		if Topic.objects.filter(user=user.id, verified=True, wiec=wiec).exists():
			personal_topics = Topic.objects.filter(user=user.id, verified=True, wiec=wiec)
		else:
			personal_topics = None

		topic_saved = False
		if request.method == "POST":

			Gform = GhostUserForm(request.POST)
			Tform = TopicForm(request.POST, instance=topic)

			if Tform.is_valid():
				Tform.save()
				topic_saved = True
			
			if Gform.is_valid():
				if User.objects.filter(username=Gform['username'].value()).exists():
					if q != None:
						return render(request, 'polls/queue.html', {'Gform': Gform, 'questions':questions, 'active' : active, 'first' : first, 'username' : f'{user.first_name} {user.last_name}', 'order' : q.number, 'topic_saved' : topic_saved,  'Tform' : Tform, 'wiec_id' : wiec.id, "personal_topics": personal_topics, "personal_announcement" : personal_announcement})
					return render(request, 'polls/queue.html', {'Gform': Gform, 'questions':questions, 'active' : active, 'first' : first, 'topic_saved' : topic_saved,  'Tform' : Tform, 'wiec_id' : wiec.id, "personal_topics": personal_topics,  "personal_announcement" : personal_announcement})
				
				lower = string.ascii_lowercase
				upper = string.ascii_uppercase
				num = string.digits
				symbols = string.punctuation

				all = lower + upper + num + symbols
				temp = random.sample(all,12)

				ghost_group, created = Group.objects.get_or_create(name='Ghost')
				user = User.objects.create_user(username=Gform['username'].value(), password="".join(temp))
				user.groups.add(ghost_group)
				user.save()

				n = len(Queue.objects.all())
				q_temp = Queue(user = user, number = n+1, now=int(time()))
				q_temp.save()
				
				first = Queue.objects.all().order_by('id').first()
				first = {"username" : "Uczestnik wiecu", "id" : first.user.id}


			if q != None:
				return render(request, 'polls/queue.html', {'Gform': Gform, 'questions':questions, 'active' : active, 'first' : first, 'username' : f'{user.first_name} {user.last_name}', 'order' : q.number, 'topic_saved' : topic_saved,  'Tform' : Tform, 'wiec_id' : wiec.id, "personal_topics": personal_topics, "personal_announcement" : personal_announcement})
			return render(request, 'polls/queue.html', {'Gform': Gform, 'questions':questions, 'active' : active, 'first' : first, 'topic_saved' : topic_saved,  'Tform' : Tform, 'wiec_id' : wiec.id, "personal_topics": personal_topics,  "personal_announcement" : personal_announcement})
		else:
			Tform = TopicForm(instance=wiec)
			Gform = GhostUserForm()

		if q != None:
			return render(request, 'polls/queue.html', {'Gform': Gform, 'questions':questions, 'active' : active, 'first' : first, 'username' : f'{user.first_name} {user.last_name}', 'order' : q.number,  'Tform' : Tform, 'wiec_id' : wiec.id, "personal_topics": personal_topics, "personal_announcement" : personal_announcement})

		return render(request, 'polls/queue.html', {'Gform': Gform, 'questions':questions, 'active' : active, 'first' : first,  'Tform' : Tform, 'wiec_id' : wiec.id, "personal_topics": personal_topics, "personal_announcement" : personal_announcement})

	#add somoeone to a queue
	def add(request):
		latest_wiec = Wiec.objects.filter(Active=True).order_by('-id').first()
		if latest_wiec.Queue_active == True:
			n = len(Queue.objects.all())
			if n==0:
				q = Queue(user = User.objects.get(username=request.user), number = n+1, now=int(time()))
			else:
				q = Queue(user = User.objects.get(username=request.user), number = n+1, now=None)
			
			q.save()
		return redirect('polls:queue')

	#delete somoeone from a queue
	def delete(request):
		q = Queue.objects.filter(user = User.objects.get(username=request.user)).first()
		
		if q != None:
			n = q.number
			q.delete()
			for i in range(n+1, len(Queue.objects.all())+2, 1):
				Q = Queue.objects.get(number=i)
				Q.number -= 1
				if Q.number == 1:
					Q.now = int(time())
				Q.save()

		return redirect('polls:queue')
	
	def delete_first(request):
		q = Queue.objects.all().first()
		n = q.number
		if q.user.groups.filter(name = "Ghost").exists():
			user = q.user
			user.delete()
		q.delete()
		for i in range(n+1, len(Queue.objects.all())+2, 1):
			Q = Queue.objects.get(number=i)
			Q.number -= 1
			if Q.number == 1:
				Q.now = int(time())
			Q.save()

		return redirect('polls:queue')

	def Open(request):
		wiec = Wiec.objects.order_by('-id').first()
		wiec.Queue_active = True
		wiec.save()
		return redirect("polls:queue")

	def Close(request):
		wiec = Wiec.objects.order_by('-id').first()
		wiec.Queue_active = False
		wiec.save()

		if Wiec.objects.filter(Queue_active=True).exists():
			wiec = Wiec.objects.filter(Queue_active=True).first()
			wiec.Queue_active = False
			wiec.save()
			
		return redirect("polls:queue")
	
	#Json used at queue.html
	def Json(request):
		queue_active = False
		if Wiec.objects.filter(Queue_active=True).exists():
			queue_active = True

		if Queue.objects.all().exists():
			queue = [[],[],[]]
			for q in Queue.objects.all():
				queue[0].append(q.number)
				if q.user.first_name == "":
					queue[1].append(q.user.username)
				else:
					queue[1].append(f'{q.user.first_name} {q.user.last_name}')

			user = User.objects.get(username=request.user)

			first = Queue.objects.all().first()
			if first.user.groups.filter(name="Ghost").exists():
				first = {"username" : "Uczestnik wiecu", "id" : first.user.id}
			else:
				if first.user.first_name == "":
					first = {"username" : first.user.username, "id" : first.user.id}
				else:
					first = {"username" : f'{first.user.first_name} {first.user.last_name}', "id" : first.user.id}

			q = Queue.objects.filter(user = user).first()
			
			if Queue.objects.filter(user=user.id).exists():
				return JsonResponse({'queue' : queue, 'Active_queqe' : queue_active, 'Order' : q.number, 'First' : first, 'Time' : int(time()) - Queue.objects.all().first().now, "exists" : Queue.objects.filter(user=user.id).first().number})
			else:
				return JsonResponse({'queue' : queue, 'Active_queqe' : queue_active, 'First' : first, 'Time' : int(time()) - Queue.objects.all().first().now, "exists" : "None"})

		else:
			return JsonResponse({'queue' : "None", 'Active_queqe' : queue_active, 'First' : "None", 'Time' : "None", "exists" : "None"})
		
#class for verifing topics and announcements
class Verify:
	#public list
	def index(request, wiec_id):
		if Wiec.objects.filter(Finnished=False).exists():
			wiec = Wiec.objects.filter(id = wiec_id).first()
			wiec_topic_list = Topic.objects.filter(wiec = wiec)
			wiec_announcement_list = Announcement.objects.filter(wiec=wiec)

		else:
			wiec_announcement_list = Announcement.objects.filter(wiec=None)
			wiec_topic_list = Topic.objects.filter(wiec=None)

		
		if 	wiec_announcement_list.filter(verified=True).exists():
			verified_announcement = wiec_announcement_list.filter(verified=True)
		else:
			verified_announcement = None

		if 	wiec_topic_list.filter(verified=True).exists():
			verified_topic = wiec_topic_list.filter(verified=True)
		else:
			verified_topic = None

		return render(request, 'polls/verify_index.html', {'verified_topic' : verified_topic, 'verified_announcement' : verified_announcement})

	#list to verify only for privileged users
	def manager_index(request, wiec_id):
		if Wiec.objects.filter(Finnished=False).exists():
			wiec = Wiec.objects.filter(id = wiec_id).first()
			wiec_topic_list = Topic.objects.filter(wiec=wiec, verified=False)
			wiec_announcement_list = Announcement.objects.filter(wiec=wiec, verified=False)
		else:
			wiec_topic_list = Topic.objects.filter(wiec=None, verified=False)
			wiec_announcement_list = Announcement.objects.filter(wiec=None, verified=False)
			

		return render(request, 'polls/verify_manager.html', {"wiec_topic_list" : wiec_topic_list, 'wiec_announcement_list' : wiec_announcement_list})

	#verifing mechanism
	def manager(request, id, to_verify):
		if to_verify == 0:
			obj_to_verify = Announcement.objects.get(id = id)
		if to_verify == 1:
			obj_to_verify = Topic.objects.get(id = id)


		if "verify" in request.POST:
			obj_to_verify.verified = True
			obj_to_verify.save()
		if "delete" in request.POST:
			obj_to_verify.delete()
		if obj_to_verify.wiec_id != None:
			return HttpResponseRedirect(reverse("polls:verifyManager_index", args=(obj_to_verify.wiec_id,)))
		else:
			return HttpResponseRedirect(reverse("polls:verifyManager_index", args=(Wiec.objects.order_by('-id').first().id,)))

class GeneratePolls:
	#Default view
	def view(request):
		question = Question(active=True, pub_date=timezone.now(), wiec=Wiec.objects.order_by('-id').first())
		
		ChoiceInlineFormSet = inlineformset_factory(Question, Choice, extra=1, can_delete=False, fields=('choice_text',))

		if request.method == "POST":

			Qform = QuestionForm(request.POST, instance=question)
			
			if Qform.is_valid():
				Qform.save()


			formset = ChoiceInlineFormSet(request.POST, request.FILES, instance=question)
			

			if formset.is_valid():
				formset.save()
				return HttpResponseRedirect(reverse('polls:results_gen', args=(question.id,)))
		else:
			Qform = QuestionForm(instance=question)

			formset = ChoiceInlineFormSet(instance=question)
		return render(request, 'polls/generate.html', {'Qform': Qform, 'formset': formset})
	
	#First vote after starting Wiec. Usually about order of topics
	def firstVote(request):
		question = Question(active=True, pub_date=timezone.now(), wiec=Wiec.objects.get(Active=True))
		
		ChoiceInlineFormSet = inlineformset_factory(Question, Choice, extra=1, can_delete=False, fields=('choice_text',))

		if request.method == "POST":

			Qform = QuestionForm(request.POST, instance=question)
			
			if Qform.is_valid():
				Qform.save()


			formset = ChoiceInlineFormSet(request.POST, request.FILES, instance=question)
			
			if formset.is_valid():
				formset.save()
			
			return HttpResponseRedirect(reverse('polls:results_gen', args=(question.id,)))
		else:
			Qform = QuestionForm(instance=question)

			formset = ChoiceInlineFormSet(instance=question)

			values = Topic.objects.filter(wiec=Wiec.objects.order_by('-id').first(), verified=True)

			first = values.order_by('id').first()

			last = values.order_by('-id').first()

		return render(request, 'polls/generate_firstVote.html', {'Qform': Qform, 'formset': formset, "values" : values, "last" : last, "first" : first})

	#only to show that poll was created.
	def results(request, question_id):
		question = Question.objects.get(id=question_id)
		return render(request, 'polls/generate_results.html', {'question': question})


class Authorization(BaseBackend):
	def login_view(request):
		if request.method == "POST":
			Wform = UserForm(request.POST)

			if Wform.is_valid():

				login = Wform.cleaned_data.get("username")
				raw_password = Wform.cleaned_data.get("password")
				if '\\' in login or '\\' in raw_password: 
					return render(request, 'registration/login.html', {'Wform' : Wform, 'Error' : "User_Does_Not_Exist"})

				user = Authorization.authenticate(request, login, raw_password)

				if user is not None:
					if check_password(raw_password, user.password) == False:
						user.set_password(raw_password)
						user.save()
					LOGIN(request, user)
					return redirect("polls:Home")
				else:
					return render(request, 'registration/login.html', {'Wform' : Wform, 'Error' : "User_Does_Not_Exist"})
		else:
			Wform = UserForm()
		return render(request, 'registration/login.html', {'Wform' : Wform})
		
	def authenticate(request, login, password):
		if User.objects.filter(username=login).exists():
			user = User.objects.filter(username=login).first()
			if user.groups.filter(name="Prezydium").exists() or user.groups.filter(name="Organizator").exists():
				user = Django_Authenticate(username=login, password=password)
				if user is not None:
					return user
				else:
					return None
					
		url="https://s35.idu.edu.pl/api/v2/auth/login"
		custom_headers = {'Content-Type' : 'application/json' , 'Accept' : 'application/json'}
		
		data = '{"login":' + '"' +  login + '"' + ',' + '"password":'  + '"' + password  + '"' + '}'

		r = requests.post(url, data=data.encode("utf-8"), headers=custom_headers)
		
		if 'token' in r.json().keys():
			token = r.json()['token']

			url="https://s35.idu.edu.pl/api/v2/user/profile"
			custom_headers = {'X-API-TOKEN' : token}
			r = requests.get(url, headers=custom_headers)

			try:
				user = User.objects.get(username=login)
			except User.DoesNotExist:
				data = r.json()['data']
				user = User.objects.create_user(username=login,password=password, first_name=data['first_name'], last_name=data['last_name'])
				user.save()

			return user
		else:
			return None

	def logout_view(request):
		logout(request)
		return redirect("polls:login")

class ErrorView:
	def PageNotFound(request):
		return render(request, 'polls/pageNotFound.html')

	def ServerError(request):
		return render(request, 'polls/serverError.html')
	
	def Forbidden(request):
		return render(request, 'polls/forbidden.html')

class Polls:
	#DetailPage Here you can vote
	def detail(request, question_id):
		q = Question.objects.get(id=question_id)
		if q.active:
			if q.ranked_system:
				return render(request, 'polls/detail_count.html', {"question" : q})
			return render(request, 'polls/detail.html', {"question" : q})
		else:
			return HttpResponseRedirect(reverse("polls:results", args=(q.id,)))

	#ResultsPage
	def results(request, pk):
		return render(request, 'polls/results.html', {"question":Question.objects.get(pk=pk)})

	#Json for ResultsPage
	def Json(request, pk):
		Data = []
		Labels = []

		for option in Question.objects.get(id=pk).choice_set.all():
			Data.append(option.votes)
			Labels.append(option.choice_text)

		return JsonResponse({'Data': Data, 'Labels': Labels,})
	
	#VotedPage waiting
	def voted(request):
		last_quest = Question.objects.order_by('-id').first().id
		return render(request, "polls/voted.html", {"last_quest" : last_quest})

	#Already voted page
	def again(request, pk):
		question = Question.objects.get(id=pk)
		return render(request, 'polls/again.html', {'question': question})
	
	#voting mechanics
	def vote(request, question_id):
		question = get_object_or_404(Question, pk=question_id)
		user = User.objects.get(username=request.user)
		if question.ranked_system == False:
			try:
				selected_choice = question.choice_set.get(choice_text=request.POST['choice'])
			except (KeyError, Choice.DoesNotExist):
				return render(request, 'polls/detail.html', {
					'question': question,
					'error_message': "You didn't select a choice.",
				})
			else:
				if question.multiple_votes:
					if Vote.objects.filter(user=user, answer=selected_choice).exists() == False:

						selected_choice.votes += 1
						selected_choice.save()
						v = Vote.objects.create(user=user, answer=selected_choice)
						v.save()
						return HttpResponseRedirect(reverse('polls:vote_again', args=(question.id,)))

					else:
						return HttpResponseRedirect(reverse('polls:again', args=(question.id,)))


				elif Vote.objects.filter(user=user, answer=selected_choice).exists() == False:
					Temp = Vote.objects.filter(user=user)
					boolean = False
					for answers in Temp:
						c = Choice.objects.get(id=answers.answer.id)
						if c.question.id == question.id:
							boolean = True

					if boolean == False:
						selected_choice.votes += 1
						selected_choice.save()
						v = Vote.objects.create(user=user,  answer=selected_choice, points=1)
						v.save()

						if question.active:
							return HttpResponseRedirect(reverse('polls:voted'))
						else:
							return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
					else:
						return HttpResponseRedirect(reverse('polls:again', args=(question.id,)))
				else:
					return HttpResponseRedirect(reverse('polls:again', args=(question.id,)))
		else:
			RankedList = request.POST['RankedList'].split(",")
			for i in range(len(RankedList)):
				choice = Choice.objects.filter(id=RankedList[i]).first()
				if Vote.objects.filter(user=user, answer=choice).exists():
					return HttpResponseRedirect(reverse('polls:again', args=(question.id,)))

			for i in range(len(RankedList)):
				choice = Choice.objects.filter(id=RankedList[i]).first()
				choice.votes += len(RankedList) - i

				choice.save()
				v = Vote.objects.create(user=user,  answer=choice, points=len(RankedList) - i)
				v.save()
					
			
			if question.active:
				return HttpResponseRedirect(reverse('polls:voted'))
			else:
				return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))	
	
	#Close poll
	def Close(request):
		question = Question.objects.filter(active=True).order_by('-id').first()
		question.active = False
		question.save()
		return redirect("polls:queue")
