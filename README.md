# Bsr_light

## Instrukcja instalacji

- Najpierw upewnij się że masz zainstalowanego pythona na swoim komputerze.
    - Wpisz komende 'python' lub 'python3' 
    - Jeżeli python3 zadziałało to używaj python3 do końca
- Upewnij się że masz gita na komputerze
    - Wpisz komende 'git'
### Pobierz repozytorium

Opcja http

- Wejdź na strone repozytorium (https://gitlab.com/BSR3/wiecos)
- Znajdź przycisk clone i wybierz https
- Teraz odpal terminal i wejdź w lokacje do której chcesz wstawić repozytorium
- Wpisz komendę 'git clone [wklej tutaj adres] / git clone https://gitlab.com/BSR3/wiecos.git'
- Teraz wejdź do repozytorium. 'cd wiecos'
- Wpisz komendę 'git checkout Django_work_env' (Zmienia ona branch na którym pracujesz)

Opcja ssh

- Wejdź na strone repozytorium (https://gitlab.com/BSR3/wiecos)
Stworzenie i skonfigurowania klucza 
- Znajdź przycisk clone i wybierz ssh
- Teraz opdal terminal i wpisz 'ssh keygen'
- Zapamiętaj gdzie go zapisujesz.
- Wejdź do tej lokacji i skopiuj cały plik który wygenerowałeś zakończony na '.pub'
- Wejdź teraz w ustawienia swojego profilu gitlaba
- Wybierz zakładke SSH Keys
- I wklej tam swój klucz
- Nadaj temu kluczowi jakiś tytuł i zapisz
- Stwórz lub edytuj plik config w swoim folderze './ssh' w którym powinieneś się właśnie znajdować
- Dodaj: 
<p>Host [Nazwa której będziemy używać do dostania się do repozytorium (np. Bsr-git)]</p>
<p>   HostName gitlab.com</p>
<p>   User git</p>
<p>   IdentityFile ~/.ssh/[lokalizacja naszego pliku z kodem ssh ale tym razem bez .pub (np.~./ssh/Bsr_rsa)]</p>

- Teraz odpal terminal i wejdź w lokacje do której chcesz wstawić repozytorium
- Wpisz komendę 'git clone [Nazwa hosta]:/[wklej tutaj adres] /(przykład) git clone Bsr-git:/git@gitlab.com:BSR3/wiecos.git'
- Teraz wejdź do repozytorium. 'cd wiecos'
- Wpisz komendę 'git checkout Django_work_env' (Zmienia ona branch na którym pracujesz)

## Instrukcja odpalania 
Upewnij się że masz zainstalowana biblioteki podane w requirements.txt w swoim lokalnym pythonie
<p>Jeżeli nie, to wpisz "pip install -r requirements.txt"</p>
<P>Wejdź w folder Bsr_light</p>

<p>Wpisz 'python manage.py runserver'</p>
<p>Wejdź w przegladarke i odpal strone "localhost:8000"</p>


